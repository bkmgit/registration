<!DOCTYPE HTML>  
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://unpkg.com/purecss@2.1.0/build/pure-min.css" integrity="sha384-yHIFVG6ClnONEA5yB5DJXfW2/KC173DIQrYoZMEtBvGzmf0PKiGyNEqe9N6BNDBH" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
</head>
<?php include 'submitdatabaseinfo.php';?>
<body>  

<?php
// define variables and set to empty values
$nameErr = $emailErr = $affiliationErr = $countryErr = "";
$name = $email = $affiliation = $country = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required / Ingiza jina";
  } else {
    $name = test_input($_POST["name"]);
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required / Ingiza barua pepe";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format / Barua pepe si sahihi";
    }
  }
    
  if (empty($_POST["affiliation"])) {
    $affiliationErr = "Affiliation is required / Ingiza kampuni au kundi";
  } else {
    $affiliation = test_input($_POST["affiliation"]);
  }

  if (empty($_POST["country"])) {
    $countryErr = "Country is required / Ingiza nchi";
  } else {
    $country = test_input($_POST["country"]);
  }

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<div class="header">
<h2>BOSSCon 2022 Registration</h2>
<img alt="Banner with DNA strand and BOSSCon 2022" src="Banner.svg" >
</div>
<div>
<h3><span class="error"> Please fill all entries / Tafadhali, andika kwa ingizo zote</span></h3>
<form method="post" action="" class="pure-form pure-form-stacked">  
  <fieldset>
  <label for="name">Name / Jina:</label> 
  <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error"> <?php echo $nameErr;?></span>
  <br>
  <label for="email">E-mail / Barua pepe:</label> 
  <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error"> <?php echo $emailErr;?></span>
  <br>
  <label for="affiliation">Affiliation / Kampuni au kundi:</label> 
  <input type="text" name="affiliation" value="<?php echo $affiliation;?>">
  <span class="error"> <?php echo $affiliationErr;?></span>
  <br>
  <label for="country">Country / Nchi:</label>
  <input type="text" name="country" value="<?php echo $country;?>">
  <span class="error"> <?php echo$countryErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</fieldset>
</form>
</div>
<?php include 'registrationsave.php';?>

</body>
</html>
